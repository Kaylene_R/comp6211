﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassExercises
{
    class Program
    {
        static void SwapNum(ref int x, ref int y)
        {

            int tempswap = x;
            x = y;
            y = tempswap;
        }

        static void Main(string[] args)
        {
             Console.WriteLine("Hello World!");

             Console.WriteLine("---------------------------------------------------------");

             Console.WriteLine("What is your name?");
             string name = Console.ReadLine();
             Console.WriteLine("Hello " + name);

             Console.WriteLine("---------------------------------------------------------");

             Console.WriteLine("You are calculating the sum of 2 numbers");
             Console.WriteLine("What is the first number? (an integer, please)");
             int add1 = Convert.ToInt32(Console.ReadLine());
             Console.WriteLine("what is the second number? (an integer please");
             int add2 = Convert.ToInt32(Console.ReadLine());
             int tot1 = add1 + add2;
             Console.WriteLine("The total is " + tot1);

             Console.WriteLine("---------------------------------------------------------");

             Console.WriteLine("You are divinding one number by another");
             Console.WriteLine("What is the first number? (an integer, please)");
             int div1 = Convert.ToInt32(Console.ReadLine());
             Console.WriteLine("what is the second number? (an integer please");
             int div2 = Convert.ToInt32(Console.ReadLine());
             int tot2 = div1 / div2;
             Console.WriteLine("The total is " + tot2);

             Console.WriteLine("---------------------------------------------------------");

             Console.WriteLine("You are calculating the product of 2 numbers");
             Console.WriteLine("Input the first number? (an integer, please)");
             int prod1 = Convert.ToInt32(Console.ReadLine());
             Console.WriteLine("Input the second number? (an integer please");
             int prod2 = Convert.ToInt32(Console.ReadLine());
             int tot3 = prod1 * prod2;
             Console.WriteLine(prod1 + " x " + prod2 + " = " + tot3);

             Console.WriteLine("---------------------------------------------------------");

             int x;
             int result;
             Console.WriteLine("You are creating a multiplication table for a number");
             Console.WriteLine("Enter a number:");
             x = Convert.ToInt32(Console.ReadLine());

             result = x * 1;
             Console.WriteLine("The table is : {0} x {1} = {2}", x, 1, result);
             result = x * 2;
             Console.WriteLine("             : {0} x {1} = {2}", x, 2, result);
             result = x * 3;
             Console.WriteLine("             : {0} x {1} = {2}", x, 3, result);
             result = x * 4;
             Console.WriteLine("             : {0} x {1} = {2}", x, 4, result);
             result = x * 5;
             Console.WriteLine("             : {0} x {1} = {2}", x, 5, result);
             result = x * 6;
             Console.WriteLine("             : {0} x {1} = {2}", x, 6, result);
             result = x * 7;
             Console.WriteLine("             : {0} x {1} = {2}", x, 7, result);
             result = x * 8;
             Console.WriteLine("             : {0} x {1} = {2}", x, 8, result);
             result = x * 9;
             Console.WriteLine("             : {0} x {1} = {2}", x, 9, result);
             result = x * 10;
             Console.WriteLine("             : {0} x {1} = {2}", x, 10, result);

             Console.WriteLine("---------------------------------------------------------");

             Console.WriteLine("You are finding the Perimeter and Area of a circle");
             Console.WriteLine("Input the radius of the circle : ");
             double rad = Double.Parse(Console.ReadLine());

             double peri = 2 * 3.14 * rad;
             double area = 3.14 * (rad * rad);

             Console.WriteLine("Perimeter = " + peri);
             Console.WriteLine("Area = " + area);

             Console.WriteLine("---------------------------------------------------------");

             int a = 100;
             int b = 500;


             Console.WriteLine("Value of a and b before sawapping");
             Console.WriteLine();
             Console.WriteLine("a=" + " " + a);
             Console.WriteLine("b=" + " " + b);
             SwapNum(ref a, ref b);
             Console.WriteLine();
             Console.WriteLine("Value of a and b after sawapping");
             Console.WriteLine();
             Console.WriteLine("a=" + " " + a);
             Console.WriteLine("b=" + " " + b);

             Console.WriteLine("---------------------------------------------------------");

             Console.WriteLine("You are finding out if a number is positive of negative");
             Console.WriteLine("Enter an integer (positive or negative)");
             int num = Convert.ToInt32(Console.ReadLine());
             if (num < 0)
              {
                  Console.WriteLine("This number is negative");
              }
             else if ( num == 0)
              {
                  Console.WriteLine("Zero is neither positive nor negative");
              }
             else
              {
                  Console.WriteLine("This number is positive ");
              }

             Console.WriteLine("---------------------------------------------------------");

             Console.WriteLine("You are find out which number is greater");
             Console.Write("Input the 1st number :");
             int num1 = Convert.ToInt32(Console.ReadLine());
             Console.Write("Input the  2nd number :");
             int num2 = Convert.ToInt32(Console.ReadLine());
             Console.Write("Input the 3rd  number :");
             int num3 = Convert.ToInt32(Console.ReadLine());

             if (num1 > num2)
              {
                 if (num1 > num3)
                  {
                      Console.Write("The 1st Number is the greatest among three.");
                  }
                 else
                  {
                      Console.Write("The 3rd Number is the greatest among three.");
                  }
              }
             else if (num2 > num3)
                {
                  Console.Write("The 2nd Number is the greatest among three.");
                }
             else
                {
                 Console.Write("The 3rd Number is the greatest among three.");
                }

             Console.WriteLine("---------------------------------------------------------");
             

             Console.WriteLine("This shows an array of randomly generated numbers");
             int[] arr = new int[5];
             Random r = new Random();

             for (int i = 0; i < 99; i++)
             {
                int rand = r.Next(0, 99);
                arr[i] = rand;
                Array.Sort(arr);
                Array.Reverse(arr);
                Console.WriteLine(arr[i]);
             }
             Console.ReadLine();

            Console.WriteLine("---------------------------------------------------------");

           Console.WriteLine("Press Enter to exit");
           Console.ReadLine();
            }
        
    }
}
